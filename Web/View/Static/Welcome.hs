module Web.View.Static.Welcome where
import Web.View.Prelude
import IHP.Environment
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A
import qualified IHP.FrameworkConfig as FrameworkConfig

data WelcomeView = WelcomeView

stylesheets = do
    when (isDevelopment FrameworkConfig.environment) [hsx|
        <link rel="stylesheet" href="/vendor/bootstrap.min.css"/>
        <link rel="stylesheet" href="/vendor/flatpickr.min.css"/>
        <link rel="stylesheet" href="/cover.css"/>
    |]
    when (isProduction FrameworkConfig.environment) [hsx|
        <link rel="stylesheet" href="/prod.css"/>
    |]

logo = [hsx|
<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bug-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M4.978.855a.5.5 0 1 0-.956.29l.41 1.352A4.985 4.985 0 0 0 3 6h10a4.985 4.985 0 0 0-1.432-3.503l.41-1.352a.5.5 0 1 0-.956-.29l-.291.956A4.978 4.978 0 0 0 8 1a4.979 4.979 0 0 0-2.731.811l-.29-.956zM13 6v1H8.5v8.975A5 5 0 0 0 13 11h.5a.5.5 0 0 1 .5.5v.5a.5.5 0 1 0 1 0v-.5a1.5 1.5 0 0 0-1.5-1.5H13V9h1.5a.5.5 0 0 0 0-1H13V7h.5A1.5 1.5 0 0 0 15 5.5V5a.5.5 0 0 0-1 0v.5a.5.5 0 0 1-.5.5H13zm-5.5 9.975V7H3V6h-.5a.5.5 0 0 1-.5-.5V5a.5.5 0 0 0-1 0v.5A1.5 1.5 0 0 0 2.5 7H3v1H1.5a.5.5 0 0 0 0 1H3v1h-.5A1.5 1.5 0 0 0 1 11.5v.5a.5.5 0 1 0 1 0v-.5a.5.5 0 0 1 .5-.5H3a5 5 0 0 0 4.5 4.975z"/>
</svg>
|]

metaTags = [hsx|
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta property="og:title" content="Quadsfit"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="TODO"/>
    <meta property="og:description" content="TODO"/>
|]

instance View WelcomeView ViewContext where
    beforeRender (context, view) = (context { layout = \view -> view }, view)
    html WelcomeView = H.docTypeHtml ! A.lang "en" $ [hsx|

  <head>
    {metaTags}
    {stylesheets}
    <style>
      html, body {
        height: 100%;
      }
    </style>
    <title>Quadsfit</title>
  </head>

  <body>
    <div class="text-center cover-page">
      <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
        <header class="masthead mb-auto">
          <div class="inner">
            <h3 class="masthead-brand bold">{logo}</h3>
            <nav class="nav nav-masthead justify-content-center">
              <a class="nav-link active" href={WelcomeAction}>Home</a>
              <a class="nav-link" href={PricingAction}>Pricing</a>
              <a class="nav-link" href="#">Login</a>
            </nav>
          </div>
        </header>

        <main role="main" class="inner cover">
          <h1 class="cover-heading">The bird.</h1>
          <p class="lead">The bird does not understand a lot, but he's trying his best. There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain.</p>
          <p class="lead">
            <a href="#" class="btn btn-lg btn-secondary">Get started</a>
          </p>
        </main>

        <footer class="mastfoot mt-auto">
          <div class="inner">
            <p>Copyright 2020 <a href={WelcomeAction}>Quadsfit</a>, by <a href="raunak">@raunak</a>.</p>
          </div>
        </footer>
      </div>
    </div>
  </body>

|]
