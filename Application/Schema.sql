-- Your database schema. Use the Schema Designer at http://localhost:8001/ to add some tables.
CREATE TYPE roles AS ENUM ('client', 'trainer', 'admin');
CREATE TYPE day_sections AS ENUM ('morning', 'evening', 'afternoon', 'all_day');
CREATE TABLE users (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    user_role roles NOT NULL,
    phone TEXT NOT NULL UNIQUE,
    availability day_sections NOT NULL
);
CREATE TABLE appointments (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    client_id UUID NOT NULL,
    trainer_id UUID NOT NULL,
    appointment_time TIMESTAMP WITH TIME ZONE NOT NULL
);
CREATE TABLE foods (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    base_amount INT DEFAULT 100 NOT NULL,
    carbs REAL NOT NULL,
    protines REAL NOT NULL,
    fats REAL NOT NULL,
    name TEXT NOT NULL
);
CREATE TYPE body_parts AS ENUM ('chest', 'legs', 'back');
CREATE TABLE exercises (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    targets body_parts[] NOT NULL
);
CREATE TABLE diets (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    diet_data BYTEA NOT NULL,
    trainer_id UUID NOT NULL,
    comments TEXT NOT NULL,
    client_ids UUID[] NOT NULL
);
CREATE TABLE workouts (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    workout_data BYTEA NOT NULL,
    comments TEXT NOT NULL,
    trainer_id UUID NOT NULL,
    client_ids UUID[] NOT NULL
);
CREATE TYPE plans AS ENUM ('simple', 'athelete');
CREATE TABLE payments (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    transaction_id TEXT NOT NULL UNIQUE,
    value INT NOT NULL,
    stamp TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
    plan plans NOT NULL,
    client_id UUID NOT NULL
);
CREATE TABLE contracts (
    id UUID DEFAULT uuid_generate_v4() PRIMARY KEY NOT NULL,
    client_id UUID NOT NULL,
    trainer_id UUID NOT NULL,
    payment_id UUID NOT NULL,
    active BOOLEAN DEFAULT true NOT NULL
);
ALTER TABLE workouts ADD CONSTRAINT workouts_ref_trainer_id FOREIGN KEY (trainer_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE contracts ADD CONSTRAINT contracts_ref_trainer_id FOREIGN KEY (trainer_id) REFERENCES users (id) ON DELETE NO ACTION;
ALTER TABLE payments ADD CONSTRAINT payments_ref_client_id FOREIGN KEY (client_id) REFERENCES users (id) ON DELETE NO ACTION;
ALTER TABLE appointments ADD CONSTRAINT appointments_ref_trainer_id FOREIGN KEY (trainer_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE appointments ADD CONSTRAINT appointments_ref_client_id FOREIGN KEY (client_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE contracts ADD CONSTRAINT contracts_ref_payment_id FOREIGN KEY (payment_id) REFERENCES payments (id) ON DELETE NO ACTION;
ALTER TABLE contracts ADD CONSTRAINT contracts_ref_client_id FOREIGN KEY (client_id) REFERENCES users (id) ON DELETE NO ACTION;
ALTER TABLE diets ADD CONSTRAINT diets_ref_trainer_id FOREIGN KEY (trainer_id) REFERENCES users (id) ON DELETE CASCADE;
