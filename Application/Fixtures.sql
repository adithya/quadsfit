

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.users DISABLE TRIGGER ALL;



ALTER TABLE public.users ENABLE TRIGGER ALL;


ALTER TABLE public.appointments DISABLE TRIGGER ALL;



ALTER TABLE public.appointments ENABLE TRIGGER ALL;


ALTER TABLE public.payments DISABLE TRIGGER ALL;



ALTER TABLE public.payments ENABLE TRIGGER ALL;


ALTER TABLE public.contracts DISABLE TRIGGER ALL;



ALTER TABLE public.contracts ENABLE TRIGGER ALL;


ALTER TABLE public.diets DISABLE TRIGGER ALL;



ALTER TABLE public.diets ENABLE TRIGGER ALL;


ALTER TABLE public.exercises DISABLE TRIGGER ALL;



ALTER TABLE public.exercises ENABLE TRIGGER ALL;


ALTER TABLE public.foods DISABLE TRIGGER ALL;



ALTER TABLE public.foods ENABLE TRIGGER ALL;


ALTER TABLE public.workouts DISABLE TRIGGER ALL;



ALTER TABLE public.workouts ENABLE TRIGGER ALL;


