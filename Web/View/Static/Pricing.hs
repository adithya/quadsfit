module Web.View.Static.Pricing where
import Web.View.Prelude

data PricingView = PricingView

instance View PricingView ViewContext where
    html PricingView = [hsx|
    <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Pricing</h1>
      <p class="lead">As a teenager, Davis learned assembly language on a Commodore 64. He earned a master's degree in electrical engineering from Arizona State University and worked for several years at Ticketmaster as a programmer for VAX machines.</p>
    </div>

      <div class="card-deck mb-3 mx-auto text-center" style="max-width: 800px;">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Layman</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$99 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li class="mb-2">Something one</li>
              <li class="mb-2">Somethig two</li>
              <li class="mb-2">Something three</li>
              <li class="mb-2">Something four</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-secondary">Get started</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm text-white bg-dark">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Athelete</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$199 <small class="">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li class="mb-2">Something one</li>
              <li class="mb-2">Somethig two</li>
              <li class="mb-2">Something three</li>
              <li class="mb-2">Something four</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-light">Get started</button>
          </div>
        </div>
      </div>

    |]
