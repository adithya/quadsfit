module Web.Controller.Static where
import Web.Controller.Prelude
import Web.View.Static.Welcome
import Web.View.Static.Pricing

instance Controller StaticController where
    action PricingAction = render PricingView
    action WelcomeAction = render WelcomeView
